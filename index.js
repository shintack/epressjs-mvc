const PORT = process.env.PORT || 4111;
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const passport = require("passport");
const session = require("express-session");
const { loginCheck } = require("./auth/passport");

// LOAD CONFIG
dotenv.config();

// AUTH PASSPORT
loginCheck(passport);

// DB CONECTION
const database = process.env.MONGODB_URL;
mongoose
  .connect(database, { useUnifiedTopology: true, useNewUrlParser: true })
  .then(() => console.log("Connect db success"))
  .catch((err) => console.log(err));

app.set("view engine", "ejs");

app.use(express.urlencoded({ extended: false }));
app.use(
  session({
    secret: "oneboy",
    saveUninitialized: true,
    resave: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());

// route
app.use("/", require("./routes/login"));

app.use("/api", require("./routes/api"));

app.listen(PORT, console.log(`Server start in port ${PORT}`));
