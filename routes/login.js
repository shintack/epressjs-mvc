const express = require("express");
const {
  registerView,
  loginView,
  registerUser,
  loginUser,
  logoutUser,
} = require("../controllers/loginController");
const { dashboardView } = require("../controllers/dashboardController");
const { protectRoute } = require("../auth/protect");

const router = express.Router();

router.get("/", loginView);

// ROUTE REGISTER
router.get("/register", registerView);
router.post("/register", registerUser);

// ROUTE LOGIN
router.get("/login", loginView);
router.post("/login", loginUser);

router.get("/logout", logoutUser);

// ROUTE DASHBOAR, WITH PROTECT ROUTE
router.get("/dashboard", protectRoute, dashboardView);

module.exports = router;
