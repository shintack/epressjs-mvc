const { name, version } = require("../package.json");
const express = require("express");
const {
  registerUser,
  loginUser,
  logoutUser,
} = require("../controllers/loginController");

const router = express.Router();

router.get("/", (req, res) => {
  res.send({
    status: 200,
    message: "OK",
    data: [
      {
        app_name: name,
        app_version: version,
      },
    ],
  });
});

router.post("/register", registerUser);
router.post("/login", loginUser);
router.get("/logout", logoutUser);

module.exports = router;
